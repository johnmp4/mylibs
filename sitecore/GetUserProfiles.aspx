<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.IO" %>

<%@ Page Language="C#" Debug="true" %>
<HTML>

<script runat="server">
string log = string.Empty;
Sitecore.Data.Database db = Sitecore.Configuration.Factory.GetDatabase("master");

public void Page_Load(object sender, EventArgs e) {
	// foreach(var prop in typeof(Sitecore.Security.Accounts.User).GetProperties()) {
	//     DoLog(prop.Name + "<br/>");
	// }
	ExportUserProfileToCSV();
}

public void ExportUserProfileToCSV() {
	var users = Sitecore.Security.Accounts.UserManager.GetUsers();
	// var users = Sitecore.SecurityModel.DomainManager.GetDomain("graininnovation").GetUsers();

	if (users != null) {
		StringBuilder sb = new StringBuilder("User Name,Email,FirstName,LastName,Title,Organization,City,State,Country,Zip,PhoneNumber,Newsletter,Crops,Role,Regions,TandCs");

		foreach(Sitecore.Security.Accounts.User user in users) {
			if (user != null) {
				if (!user.Name.StartsWith("graininnovation\\")) continue;

				var user1 = Sitecore.Security.Accounts.User.FromName(user.Name, true);
				var profile = user1.Profile;

				var crops = profile["Crops"].Split('|').Select(c => GetItemVal(c, "Title")).ToArray();
				var roles = profile["Role"].Split('|').Select(c => GetItemVal(c, "Value")).ToArray();
				var regions = profile["Regions"].Split('|').Select(c => GetItemVal(c, "Title")).ToArray();

				// if (sb.ToString() == "User Name"){
				// 	foreach(var prop in user1.Profile.GetType().GetProperties())
				// 		sb.Append("," + prop.Name);
				// }

				string lines = string.Format("\r\n\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\"",
				user1.Name,
				user1.LocalName,
				profile["FirstName"],
				profile["LastName"],
				profile["Title"],
				profile["Organization"],
				profile["City"],
				profile["State"],
				profile["Country"],
				profile["Zip"],
				profile["PhoneNumber"],
				profile["Newsletter"],
				string.Join("|", crops),
				string.Join("|", roles),
				string.Join("|", regions),
				profile["TandCs"]);
				sb.Append(lines);
			} else
				DoLog("User is null");
		}

		WriteToCSV(sb);
	} else
		DoLog("users are null");
}

public string GetItemVal(string id, string field) {
	if (id == "") return id;
	var item = db.GetItem(Sitecore.Data.ID.Parse(id));
	if (item == null) return id;
	return item[field];
}

public void WriteToCSV(StringBuilder sb) {

	// string appPath = Request.PhysicalApplicationPath;
	string filePath = Server.MapPath("~/sitecore/admin/_GI_Profiles.csv");

	//stringbuilder to physical csv file
	using(StreamWriter swriter = new StreamWriter(filePath)) {
		swriter.Write(sb.ToString());
	}

}
public void DoLog(string message) {
	Response.Write(message + "\r\n");
}
</script>

<body>
</body>
</HTML>