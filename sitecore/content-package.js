const path = require('path')
const fs = require('fs-extra')
const glob = require('glob')
const parser = require('fast-xml-parser')

let packagePath

class ContentPackage {
    constructor(initialPath) {
        if (initialPath) {
            this.packagePath = path.resolve(initialPath)
        }

        this.data = {}
    }

    set path(newPath) {
        this.packagePath = path.resolve(newPath)
    }

    get path() {
        return this.packagePath
    }

    async parse() {
        try {
            let stats = await fs.stat(this.path)

            if (stats.isFile()) {
                this.data = await parseFile(this.path)
            } else if (stats.isDirectory()) {
                this.data = await parseFiles(this.path)
            }
        } catch (error) {
            console.log(error)
        }

        return this
    }

    toString() {
        return JSON.stringify(this.data, null, 2)
    }

    language(lang) {
        if (this.data.items) {
            this.data.items = this.data.items.filter(item => {
                if (typeof lang == 'string')
                    lang = `^(${lang})$`
                return new RegExp(lang, 'i').test(item['@_language'])
            })
        }

        return this
    }

    latest() {
        if (this.data.items) {
            this.data.items = this.data.items.reduce((accumulator, val) => {
                const current = accumulator.findIndex(item => item['@_id']==val['@_id'])

                if (current < 0) {
                    accumulator.push(val)
                } else if (accumulator[current]['@_version'] < val['@_version']) {
                    accumulator[current] = val
                }

                return accumulator
            }, [])

            return this
        }
    }

    reduceFields(filter) {
        if (this.data.items) {
            this.data.items = this.data.items.map(item => reduceItemFields(item, filter))
        } else {
            this.data = reduceItemFields(this.data, filter)
        }

        return this
    }

    filter(filter) {
        if (this.data.items) {
            this.data.items = this.data.items.map(item => filterProps(item, filter))
        } else {
            this.data = filterProps(this.data, filter)
        }

        return this
    }

    toCSV() {
        let data = this.data.items || [this.data]
        let attributes = [], fields = []

        data.forEach(item => {
            for (let prop in item) {
                if (prop == 'fields') continue
                if (item.hasOwnProperty(prop)) {
                    prop = prop.replace('@_', '')
                    if (!attributes.includes(prop)) attributes.push(prop)
                }
            }

            for (const field in item.fields) {
                if (item.fields.hasOwnProperty(field) && !fields.includes(field)) {
                    fields.push(field)
                }
            }
        })

        return data.reduce((accumulator, val) => {
            const columns = []
            
            attributes.forEach(attr => {
                columns.push(val['@_' + attr])
            })

            fields.forEach(field => {
                columns.push(val.fields[field])
            })

            accumulator+= `\r\n"${columns.join('","')}"`
            return accumulator
        }, attributes.concat(fields).join(','))
    }
}

module.exports = ContentPackage

function filterProps(item, filter) {
    for (const prop in item) {
        if (prop == 'fields') continue
        if (item.hasOwnProperty(prop)) {
            if (!filter(prop)) delete item[prop]
        }
    }

    return item
}

function reduceItemFields(item, filter) {
    item.fields = item.fields.field.filter(filter, item)
    item.fields = item.fields.reduce((accumulator, val) => {
        switch (val['@_type']) {
            case 'Image':
                let id = val.content.match(/mediaid="(\{.*?\})"/)
                if (id && id.length == 2) val.content = id[1]
                break
        }

        accumulator[val['@_key']] = val.content
        return accumulator
    }, {})
    return item
}

async function parseFiles(dir) {
    let files, items

    try {
        files = await getFiles(dir)
        items = await Promise.all(files.map(async file => {
            let item = (await parseFile(path.resolve(path.join(dir, file)))).item
            return Object.assign({
                '@_path': file.replace(new RegExp('/?' + item['@_id'] + '.*'), '')
            }, item)
        }))
    } catch (e) {
        console.log(e)
    }

    return { items: items }
}

function getFiles(dir) {
    return new Promise((resolve, reject) => {
        glob('**/xml', {
            cwd: path.resolve(dir)
        }, (err, matches) => {
            if (err) {
                reject(err)
            }

            resolve(matches)
        })
    })
}

async function parseFile(file) {
    if (typeof file === 'string') {
        try {
            file = await fs.readFile(file, 'utf8')
        } catch (e) {
            console.log('FILE PARSE ERROR:', e)
        }
    }

    return parser.parse(file, { ignoreAttributes: false })
}
