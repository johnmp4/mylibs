#!/usr/bin/env node

const fs = require('fs-extra')
const url = require('url')
const path = require('path')

if (process.argv.length < 3) {
    console.log('Input file not provided; exiting.')
    process.exit(1)
}

console.log((inFile => {
     try {
         inFile = fs.readFileSync(path.resolve(inFile), 'utf8')
         inFile = inFile.trim().split(/[\r\n]+/)

         return getNodes(inFile.map(redirect => {
             redirect = redirect.split(/\s+/)
             return { source: redirect[0], destination: redirect[1] }
         }))
     } catch (error) {
         console.log(error)
     }
})(process.argv[2]))

function getNodes(list = []) { // [{ source: 'http://absolute', destination: 'http://absolute' },]
    let nodes = ''

    list.forEach(redirect => {
        redirect.source = url.parse(redirect.source)
        redirect.destination = url.parse(redirect.destination)

        nodes+= `
    <location path="${redirect.source.pathname.substring(1)}">
        <system.webServer>
            <httpRedirect enabled="true" destination="${redirect.destination.href}" httpResponseStatus="Permanent" />
        </system.webServer>
    </location>`

    })

    return nodes
}
