#!/usr/bin/env node

const RedirectPolicy = require('mylibs/akamai/cloudlets/redirector/policy')
const paths = [
    "/some/url/path",
    "/another/path"
]

consolidate(...process.argv.slice(2)).catch(e => console.error(e))
// createUpload(process.argv[2], process.argv[3]).catch(e => console.error(e))

async function createUpload(infile, outfile) {
    let policy = await RedirectPolicy.fromTxt(infile)
    console.log(await RedirectPolicy.toCsv(outfile, policy))
}

async function findRelated(infile, outfile) {
    let policy
    policy = await RedirectPolicy.fromCsv(infile)
    console.log(await RedirectPolicy.toCsv(outfile, policy.filterPaths(paths,/* checkDest */ true,/* invert */ true)))
}

async function consolidate(outfile, ...infiles) {
    infiles = await Promise.all(infiles.map(infile => {
        if (/\.txt$/.test(infile)) return RedirectPolicy.fromTxt(infile)
        return RedirectPolicy.fromCsv(infile)
    }))

    let policy = infiles.reduce((policy, infile) => {
        if (!policy) return infile
        return policy.append(infile)
    }, false)

    console.log(await RedirectPolicy.toCsv(outfile, policy.expandPaths()))
}
