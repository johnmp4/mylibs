const fs = require('fs-extra')
const path = require('path')
const URL = require('url').URL

class RedirectPolicy {

    constructor(source, ...rules) {
        if (arguments.length = 1) {
            rules = source.rules
            source = source.versionInfo
        }

        this.rules = rules || []
        this.versionInfo = source
    }

    get length() {
        return this.rules.length
    }

    static get columns() {
        return {
            ruleName: 'ruleName',
            host: 'host',
            path: 'path',
            result: {
                redirectURL: 'result.redirectURL',
                statusCode: 'result.statusCode'
            }
        }
    }

    filter() {
        let initialLength = this.length

        this.rules = this.rules.filter(...arguments)
        
        this.versionInfo+= `\r\n# Custom filter;`
        this.versionInfo+= `\r\n"# Rules: ${initialLength} (initial), ${this.length} (filtered)" `
        this.versionInfo+= "\r\n# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"

        return this
    }

    filterPaths(paths = [], checkDest = true, invert = false) {
        let initialLength = this.length

        this.rules = this.rules.filter(rule => {
            const include = paths.filter(path => {
                return rule[RedirectPolicy.columns.path].includes(path)
                    || checkDest && rule[RedirectPolicy.columns.result.redirectURL].includes(path)
            }).length

            return invert ? !include : include
        })

        this.versionInfo+= `\r\n# Filtered Paths (${(checkDest ? 'destinations CHECKED' : 'destinations NOT CHECKED')});`
        this.versionInfo+= `\r\n# - ${paths.join("\r\n# - ")}`
        this.versionInfo+= `\r\n# Rules: ${initialLength} (initial), ${this.length} (filtered), `
        this.versionInfo+= "\r\n# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"

        return this
    }
    
    addRedirect(source, destination, name, status = 301, merge = false) {
        if (!/\*$/.test(source.pathname)) {
            let rule = this.rules.findIndex(
                rule => rule[RedirectPolicy.columns.result.redirectURL]==destination.href)

            if (rule >= 0
                && (merge || this.rules[rule][RedirectPolicy.columns.ruleName]==name)
                && this.rules[rule][RedirectPolicy.columns.result.statusCode]==status) {
                if (!this.rules[rule][RedirectPolicy.columns.host].split(' ').includes(source.host))
                    this.rules[rule][RedirectPolicy.columns.host]+= ' ' + source.host

                if (!this.rules[rule][RedirectPolicy.columns.path].split(' ').includes(source.pathname))
                    this.rules[rule][RedirectPolicy.columns.path]+= ' ' + source.pathname

                return rule
            }
        }

        return this.rules.push({
            [RedirectPolicy.columns.ruleName]: name,
            [RedirectPolicy.columns.host]: source.host,
            [RedirectPolicy.columns.path]: source.pathname,
            [RedirectPolicy.columns.result.redirectURL]: destination.href,
            [RedirectPolicy.columns.result.statusCode]: status
        })
    }

    append(rules) {
        if (rules instanceof RedirectPolicy) {
            this.versionInfo+= "\n" + rules.versionInfo
            rules = rules.rules
        }

        if (!rules instanceof Array) rules = [rules]
        this.rules = this.rules.concat(rules)
        return this
    }

    expandPaths() {
        this.rules = this.rules.reduce((newRules, oldRule) => {
            if (oldRule[RedirectPolicy.columns.path]) {
                const paths = oldRule[RedirectPolicy.columns.path].split(' ')
                paths.forEach(path => {
                    newRules.push(Object.assign({}, oldRule, {
                        [RedirectPolicy.columns.path]: path
                    }))
                })
            } else {
                newRules.push(oldRule)
            }

            return newRules
        }, [])

        return this
    }

    static async toCsv(csv, rules) {
        let columns = []
        let versionInfo = rules.versionInfo ? rules.versionInfo + "\r\n" : ''
        csv = path.resolve(csv)

        if (rules instanceof RedirectPolicy) rules = rules.rules
        rules = rules.reduce((output, rule) => {
            for (const key in rule) {
                if (rule.hasOwnProperty(key)) {
                    if (!columns.includes(key)) columns.push(key)
                }
            }

            columns.forEach((column, i) => {
                if (i===0) output+= "\r\n"
                else output+= ','
                output+= `"${rule[column]}"`
            });

            return output
        }, '')

        await fs.outputFile(csv, versionInfo + columns.join(',') + rules, 'utf8')
        return 'Wrote CSV to ' + csv
    }

    static async fromTxt(txt, separator) {
        txt = path.resolve(txt)
        let versionInfo = {
            file: path.basename(txt, '.txt'),
            date: new Date(),
            count: 0
        }

        txt = await fs.readFile(txt, 'utf8')
        txt = txt.trim().split(/[\r\n]+/)

        txt = txt.reduce((policy, line, i) => {
            if (/^\s*$/.test(line)) return policy
            if (/^#/.test(line)) {
                policy.versionInfo+= "\n" + line
                return policy
            }

            versionInfo.count++

            try {
                let rule = line.trim().split(/\s+/)

                policy.addRedirect(
                    new URL(rule[0]),
                    new URL(rule[1]),
                    `Import ${versionInfo.file} ${versionInfo.date.toDateString()}`,
                    rule[2]
                )
            } catch (error) {
                console.log(`Error parsing redirect list at line: ${i+1} -- ${line}`)
                throw error
            }

            return policy
        }, new RedirectPolicy({ 
            versionInfo: '',
            rules: [],
            columns: [
                RedirectPolicy.columns.ruleName,
                RedirectPolicy.columns.host,
                RedirectPolicy.columns.path,
                RedirectPolicy.columns.result.redirectURL,
                RedirectPolicy.columns.result.statusCode
            ]
        }))

        txt.versionInfo = `#
# Generated from source -> destination list
# ${versionInfo.file}
# ${versionInfo.date.toLocaleString()}
# ${versionInfo.count} redirects in list
# ${txt.length} rules in output
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -${txt.versionInfo}`

        return txt
    }

    static async fromCsv(csv) {
        csv = path.resolve(csv)
        csv = await fs.readFile(csv, 'utf8')

        return new RedirectPolicy(
            csv.trim().split(/[\r\n]+/).reduce((policy, line) => {
                if (/^#/.test(line)) {
                    policy.versionInfo+= (policy.versionInfo.length ? "\n" : '') + line
                    return policy
                }

                if (new RegExp('^' + RedirectPolicy.columns.ruleName).test(line)) {
                    policy.columns = line.split(',')
                    return policy
                }

                policy.rules.push(line.split(',').reduce((rule, field, i) => {
                    rule[policy.columns[i]] = field
                    return rule
                }, {}))

                return policy
            }, { versionInfo: '', rules: [], columns: [] })
        )
    }
}

module.exports = RedirectPolicy
